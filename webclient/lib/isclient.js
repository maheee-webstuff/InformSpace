function ISClient() {
  this.session = null;
  this.onConnected = null;
  this.onAuthorized = null;
  this.onAuthorizationFailure = null;
  this.onDisconnected = null;
}


ISClient.prototype._cbWrap = function (cp) {
  var that = this;
  return function () {
    cp.apply(that, arguments);
  };
};

ISClient.prototype._onConnected = function () {
  console.log("ISClient: Connected!");
  if (this.onConnected) {
    this.onConnected.apply(this, arguments);
  }
};

ISClient.prototype._onAuthorized = function () {
  this.session.prefix("doc", "ws://is/doc");
  
  console.log("ISClient: Authorized!");
  if (this.onAuthorized) {
    this.onAuthorized.apply(this, arguments);
  }  
};

ISClient.prototype._onAuthorizationFailure = function () {
  console.log("ISClient: Authorization failed!");
  if (this.onAuthorizationFailure) {
    this.onAuthorizationFailure.apply(this, arguments);
  }
};

ISClient.prototype._onDisconnected = function (code, reason) {
  console.log("ISClient: Disconnected!");
  console.log("ISClient: Reason: " + reason + " (" + code + ")");
  if (this.onDisconnected) {
    this.onDisconnected.apply(this, arguments);
  }
};


ISClient.prototype.calcSignature = function (challenge, password) {
  var ch = JSON.parse(challenge);
  var hashedPassword = ab.deriveKey(password, ch.authextra.account);
  var secret = ab.deriveKey(hashedPassword, ch.authextra.session);
  return this.session.authsign(challenge, secret);
};

ISClient.prototype.authenticate = function (username, password) {
  var that = this;

  this.session.authreq(username).then(function (challenge) {
    var signature = that.calcSignature(challenge, password);

    that.session.auth(signature).then(
      that._cbWrap(that._onAuthorized),
      that._cbWrap(that._onAuthorizationFailure)
    );
  }, that._cbWrap(that._onAuthorizationFailure));
};

ISClient.prototype.connect = function (url, username, password) {
  var that = this;
  ab.connect(url,
    function (session) {
      that.session = session;

      that._cbWrap(that._onConnected());
      that.authenticate(username, password);
    },
    that._cbWrap(that._onDisconnected)
  );
};


ISClient.prototype.subscribe = function (documentId, eventCallback) { //callback(topicUri, event)
  this.session.subscribe("doc:/events#" + documentId, eventCallback);
};

ISClient.prototype.unsubscribe = function (documentId) {
  this.session.unsubscribe("doc:/events#" + documentId);
};


ISClient.prototype.createRPCErrorHandler = function (next) {
  return function (error) {
    console.log("ISClient: A RPC error occured!");
    console.log("ISClient: URI: " + error.uri);
    console.log("ISClient: Description: " + error.desc);
    console.log("ISClient: Details: " + error.details);
    if (next) {
      next.apply(this, arguments);
    }
  }
}


ISClient.prototype.rpc0 = function (action, documentId, onSuccess, onError) {
  this.session.call("doc:#" + action, documentId).then(onSuccess, this.createRPCErrorHandler(onError));
};

ISClient.prototype.rpc1 = function (action, documentId, arg1, onSuccess, onError) {
  this.session.call("doc:#" + action, documentId, arg1).then(onSuccess, this.createRPCErrorHandler(onError));
};


ISClient.prototype.get = function (documentId, onSuccess, onError) {
  this.rpc0("get", documentId, onSuccess, onError);
};

ISClient.prototype.lock = function (documentId, onSuccess, onError) {
  this.rpc0("lock", documentId, onSuccess, onError);
};

ISClient.prototype.unlock = function (documentId, onSuccess, onError) {
  this.rpc0("unlock", documentId, onSuccess, onError);
};

ISClient.prototype.save = function (documentId, newContent, onSuccess, onError) {
  this.rpc1("save", documentId, newContent, onSuccess, onError);
};
