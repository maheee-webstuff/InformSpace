import sys
import uuid
import optparse
from autobahn.wamp import WampCraProtocol


def buildOptionParser():
  parser = optparse.OptionParser()
  parser.add_option("-s", "--salt",
                    action="store", type="string",
                    dest="salt", default=str(uuid.uuid4()),
                    help="Salt for hashing, random if none is given")
  parser.add_option("-k", "--keylen",
                    action="store", type="int",
                    dest="keylen", default=32,
                    help="keylen")
  parser.add_option("-i", "--iterations",
                    action="store", type="int",
                    dest="iterations", default=1000,
                    help="iterations")
  return parser

def createHash(secret, salt, keylen, iterations):
  authExtra = {
    'salt': salt,
    'keylen': keylen,
    'iterations': iterations
  }
  return {
    #'authKey': authKey,
    'hashedSecret': WampCraProtocol.deriveKey(secret, authExtra),
    'authExtra': authExtra
  }


def main(argv=None):
  argv = argv or sys.argv
  (options, args) = buildOptionParser().parse_args(argv)
  
  secret = args[1]
  
  print(createHash(secret, options.salt, options.keylen, options.iterations))


if __name__ == '__main__':
  sys.exit(main())