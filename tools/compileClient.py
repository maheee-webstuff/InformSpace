import sys
import optparse
import re
import os


def buildOptionParser():
  parser = optparse.OptionParser()
  parser.add_option("-b", "--baseDir",
                    action="store", type="string",
                    dest="baseDir", default="../webclient/",
                    help="Base Directory")
  parser.add_option("-i", "--inputFile",
                    action="store", type="string",
                    dest="inputFile", default="index_dev.html",
                    help="Input File (HTML)")
  parser.add_option("-o", "--outputFile",
                    action="store", type="string",
                    dest="outputFile", default="index.html",
                    help="Output File (HTML)")
  return parser


#<script src="./lib/creole/creole.js"></script>
#<link rel="stylesheet" href="./lib/fontawesome/css/font-awesome.min.css">

scriptRe = re.compile('^<script\s+src="([^"]+)".*')
styleRe = re.compile('^<link.+href="([^"]+)".*')

def outputFile(out, filename):
  file = open(filename)
  for line in file:
    line = line.strip()
    if len(line) > 0:
      out.write(line + '\n')
  file.close()

def outputScriptFile(out, filename):
  out.write('<script>' + '\n')
  outputFile(out, filename)
  out.write('</script>' + '\n')

def outputStyleFile(out, filename):
  out.write('<style>' + '\n')
  outputFile(out, filename)
  out.write('</style>' + '\n')

def main(argv=None):
  argv = argv or sys.argv
  (options, args) = buildOptionParser().parse_args(argv)
  
  inputFile = os.path.join(options.baseDir, options.inputFile)
  outputFile = os.path.join(options.baseDir, options.outputFile)
  
  out = open(outputFile, 'w')

  for line in open(inputFile):
    line = line.strip()
    scriptMatch = scriptRe.match(line)
    styleMatch = styleRe.match(line)
    if scriptMatch:
      outputScriptFile(out, os.path.join(options.baseDir, scriptMatch.group(1)))
    elif styleMatch:
      outputStyleFile(out, os.path.join(options.baseDir, styleMatch.group(1)))
    elif len(line) > 0:
      out.write(line+'\n')

  out.close()

if __name__ == '__main__':
  sys.exit(main())