import sys
import optparse
import uuid
from hashSecret import createHash
from pymongo import MongoClient

def buildOptionParser():
  parser = optparse.OptionParser()
  parser.add_option("-s", "--salt",
                    action="store", type="string",
                    dest="salt", default=str(uuid.uuid4()),
                    help="Salt for hashing, random if none is given")
  parser.add_option("-k", "--keylen",
                    action="store", type="int",
                    dest="keylen", default=32,
                    help="keylen")
  parser.add_option("-i", "--iterations",
                    action="store", type="int",
                    dest="iterations", default=1000,
                    help="iterations")
  parser.add_option("-d", "--database",
                    action="store", type="string",
                    dest="database", default='mongodb://localhost/informspace',
                    help="Database URL")
  return parser


def createUser(username, password, salt, keylen, iterations):
  authInfo = createHash(password, salt, keylen, iterations)
  authInfo['authKey'] = username
  return {
    '_id': username,
    'auth': authInfo
  }


def main(argv=None):
  argv = argv or sys.argv
  (options, args) = buildOptionParser().parse_args(argv)
  
  username = args[1]
  password = args[1]
  
  user = createUser(username, password, options.salt, options.keylen, options.iterations)
  print(user)
  
  client = MongoClient(options.database)
  db = client.get_default_database()
  collection = db['users']
  collection.insert(user)
  
  client.disconnect()


if __name__ == '__main__':
  sys.exit(main())