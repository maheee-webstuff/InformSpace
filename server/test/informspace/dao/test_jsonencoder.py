import unittest
import informspace.dao.encoding as encoding
import json


@encoding.dictFields(['testdata'])
class Metadata(object):
  def __init__(self):
    self.testdata = "Hello World"


@encoding.dictFields(['title', 'lastChange', 'meta', 'active', 'empty'])
class Document(object):
  def __init__(self):
    self.title = "my title"
    self.content = "my content"
    self.lastChange = 123456
    self.active = True
    self.meta = Metadata()
    self.empty = None


class JsonEncoderTestCase(unittest.TestCase):

  def setUp(self):
    pass
  
  def test_encodingOfDocument(self):
    """Testing encoding of document dao"""
    
    doc = Document()
    
    result = encoding.toJson(doc)
    print(result)
    
    self.assertIn('"lastChange": 123456', result)
    self.assertIn('"meta": {"testdata": "Hello World"}', result)
    self.assertIn('"title": "my title"', result)
    self.assertIn('"active": true', result)



suite = unittest.TestLoader().loadTestsFromTestCase(JsonEncoderTestCase)

if __name__ == '__main__':
  unittest.main()

