import logging

from yapsy.PluginManager import PluginManager

import informspace.pluginmultimanager as pmm
import informspace.pluginclasses as pc


def load(places=["plugins"], excludePlugins={'authmngr':[], 'docstore':[], 'notifmngr': []}):
  logger = logging.getLogger("plugin")

  def getPluginSetup(pluginConfig, option, default):
    if pluginConfig.has_section("Setup") and pluginConfig.has_option("Setup", option):
      return pluginConfig.get("Setup", option)
    return default

  def enrichPlugin(pluginInfo):
    pluginInfo.plugin_object.name = pluginInfo.name
    pluginInfo.plugin_object.priority = -int(getPluginSetup(pluginInfo.details, "Priority", 0))
    pluginInfo.plugin_object.namespaces = getPluginSetup(pluginInfo.details, "Namespace", "").split(",")
    
    pluginInfo.plugin_object.namespaces = [e.strip() for e in pluginInfo.plugin_object.namespaces]

  def logPlugin(plugin_object, used):
    used = used and "-" or "x"
    logger.info(" %s %-20s %-5s %s", used,plugin_object.name, -plugin_object.priority, plugin_object.namespaces)

  def checkPlugins(category, excludedPlugins, collection):
    for pluginInfo in manager.getPluginsOfCategory(category):
      enrichPlugin(pluginInfo)
      if not pluginInfo.name in excludedPlugins:
        logPlugin(pluginInfo.plugin_object, True)
        collection.add(pluginInfo.plugin_object)
      else:
        logPlugin(pluginInfo.plugin_object, False)

  authentificationManager = pmm.MultiAuthentificationManager()
  documentStore = pmm.MultiDocumentStore()
  notificationManager = pmm.MultiNotificationManager()

  manager = PluginManager()
  manager.setCategoriesFilter({
    "AuthentificationManager": pc.AuthentificationManagerPlugin,
    "DocumentStore": pc.DocumentStorePlugin,
    "NotificationManager": pc.NotificationManagerPlugin
  })
  manager.getPluginLocator().setPluginPlaces(places)
  manager.collectPlugins()

  logger.info("Found AuthentificationManagers:")
  checkPlugins("AuthentificationManager", excludePlugins['authmngr'], authentificationManager)

  logger.info("Found DocumentStore:")
  checkPlugins("DocumentStore", excludePlugins['docstore'], documentStore)

  logger.info("Found NotificationManagers:")
  checkPlugins("NotificationManager", excludePlugins['notifmngr'], notificationManager)

  return (authentificationManager, documentStore, notificationManager)