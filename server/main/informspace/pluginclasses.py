from yapsy.IPlugin import IPlugin
import informspace.dao.data as data


class AuthentificationManagerPlugin(IPlugin):
  def getAuthentificationInformation(self, authKey):
    return data.Result(False, "AuthInfo not found!")


class DocumentStorePlugin(IPlugin):
  def get(self, userinfo, documentId):
    return data.Result(False, "Document not found!")
  
  def save(self, userinfo, documentId, content):
    return data.Result(False, "Saving document failed!")
  
  def lock(self, userinfo, documentId):
    return data.Result(False, "Locking document failed!")
  
  def unlock(self, userinfo, documentId):
    return data.Result(False, "Unlocking document failed!")


class NotificationManagerPlugin(IPlugin):
  pass
