import logging

import informspace.dao.encoding as enc

from autobahn.wamp1.protocol import exportRpc


class DocIf(object):
  
  logger = logging.getLogger("docif")
  
  def __init__(self, userInfo, documentStore, notificationManager, eventDispatcher):
    self.userInfo = userInfo
    self.documentStore = documentStore
    self.notificationManager = notificationManager
    self.eventDispatcher = eventDispatcher

  @exportRpc
  def get(self, documentId):
    #self.eventDispatcher(documentId, {'action': "get"})
    return enc.toDict(self.documentStore.get(self.userInfo, documentId))

  @exportRpc
  def lock(self, documentId):
    #self.eventDispatcher(documentId, {'action': "lock"})
    return enc.toDict(self.documentStore.lock(self.userInfo, documentId))

  @exportRpc
  def unlock(self, documentId):
    #self.eventDispatcher(documentId, {'action': "unlock"})
    return enc.toDict(self.documentStore.unlock(self.userInfo, documentId))

  @exportRpc
  def save(self, documentId, content):
    #self.eventDispatcher(documentId, {'action': "save"})
    return enc.toDict(self.documentStore.save(self.userInfo, documentId, content))
