import logging

import informspace.dao.data as data
import informspace.pluginclasses as p


class DefaultDocumentStore(p.DocumentStorePlugin):
  
  logger = logging.getLogger("docstore")
  
  def splitDocumentId(self, documentId):
    s = filter(None, documentId.split(':'))
    if len(s) >= 1:
      return (':'.join(s[:-1]), s[-1:][0])
  
  def createDocument(self, documentId, content):
    (parent, name) = self.splitDocumentId(documentId)

    doc = data.Document()

    doc.id = documentId
    doc.name = name
    doc.parent = parent
    doc.fullname = documentId
    doc.content = content

    self.documents[documentId] = doc
  
  def __init__(self):
    self.documents = {}
    
    self.createDocument('system:markup',"""
== Simple Stuff ==
**Bold** //Italics//

== Heavy Stuff ==
=== Horizontal Line ===
----
=== Lists ===
* Item 1
* Item 2
** Subitem 1

# Item 1
# Item 2
## Subitem 1

=== Links ===
Link to here: [[system:markup|Markup]], [[system:markup]]

Link to somewhere else: [[system:info|Info]], [[system:info]]

Link to Google: [[http://www.google.de|Google]], [[http://www.google.de]]

=== Table ===
|=table|=header|
|table|row|
|table|row|

=== No Wiki ===
{{{**Not Bold**}}}

""")
  
  def get(self, userinfo, documentId):
    if documentId in self.documents:
      return data.Result(True, None, self.documents[documentId])
    return data.Result(False, "Document not found!") #TODO that shouldn't be possible
  
  def save(self, userinfo, documentId, content):
    return data.Result(False, "")
  
  def lock(self, userinfo, documentId):
    return data.Result(False, "")
  
  def unlock(self, userinfo, documentId):
    return data.Result(True, "")
