import informspace.pluginclasses as p
import informspace.dao.data as data

class DefaultAuthentificationManager(p.AuthentificationManagerPlugin):
  
  def getAuthentificationInformation(self, authKey):
    if authKey == 'guest':
      # secret is 'guest'
      user = data.User()
      user.id = authKey
      user.auth = {
        'authKey': authKey,
        'hashedSecret': 'UitJFmfyjKmL+LnEB9FgH0yjagx0LQFIegO8r1oujr4=',
        'authExtra': {
          'keylen': 32,
          'iterations': 1000,
          'salt': '3283dd49-dbbe-4e07-a04b-ef4174a9c4cb'
        }
      }
      return data.Result(True, None, user)

    return None
