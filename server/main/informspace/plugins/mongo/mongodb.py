import pymongo


class MongoDb(object):

  url='mongodb://localhost/informspace'
  client = None
  db = None

  def init(self):
    if self.db is None or self.db is None:
      self.client = pymongo.MongoClient(self.url)
      self.db = self.client.get_default_database()
  
  def getUser(self, username):
    self.init()
    return self.db.users.find_one({'_id': username})

  def getDocument(self, documentId):
    self.init()
    return self.db.documents.find_one({'_id': documentId})
  
  def saveDocument(self, documentId, document):
    self.init()
    return self.db.documents.update(
      {'_id': documentId},
      {'$set': document},
      upsert=True, multi=False, w=1
    )
    return res

  def updateDocument(self, documentId, userId, document):
    self.init()
    return self.db.documents.update(
      {'$and': [
        {'_id': documentId},
        {'$or': [
          {'locked.by': {'$exists': False}},
          {'locked.by': userId} ] }]},
      {'$set': document},
      upsert=False, multi=False, w=1
    )
