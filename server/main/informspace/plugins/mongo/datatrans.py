import informspace.dao.data as data


def mongo2document(mdoc):
  doc = data.Document()

  doc.id = str(mdoc['_id'])
  doc.name = str(mdoc['name'])
  doc.parent = str(mdoc['parent'])
  doc.fullname = str(mdoc['fullname'])
  if mdoc['created'] and mdoc['created']['by']:
    doc.created = data.Metadata()
    doc.created.set(str(mdoc['created']['at']), str(mdoc['created']['by']))
  if mdoc['locked'] and mdoc['locked']['by']:
    doc.locked = data.Metadata()
    doc.locked.set(str(mdoc['locked']['at']), str(mdoc['locked']['by']))
  doc.content = str(mdoc['content'])
  doc.linked = str(mdoc['linked'])
  doc.history = str(mdoc['history'])
  
  return doc
