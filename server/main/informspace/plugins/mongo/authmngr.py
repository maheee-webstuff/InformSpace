import informspace.pluginclasses as p
import informspace.dao.data as data
import mongodb
import datatrans
import time


class MongoAuthentificationManger(p.AuthentificationManagerPlugin):

  def getAuthentificationInformation(self, authKey):
    """Plugin-Interface: returns authentification information retrieved from MongoDb """
    muser = self.db.getUser(authKey)
    if muser is not None:
      user = data.User()
      user.id = authKey
      user.auth = muser['auth']
      return data.Result(True, None, user)

    return data.Result(False, "AuthInfo not found!")
