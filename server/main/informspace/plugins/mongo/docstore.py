import informspace.pluginclasses as p
import informspace.dao.data as data
import mongodb
import datatrans
import time
import logging

class MongoDocumentStore(p.DocumentStorePlugin):
  
  logger = logging.getLogger("mongo")

  def splitDocumentId(self, documentId):
    s = filter(None, documentId.split(':'))
    if len(s) >= 1:
      return (':'.join(s[:-1]), s[-1:][0])

  def extractLinked(self, content):
    # TODO
    return []
  
  def fixres(self, res, ignoreUpdateMsg):
    if res and res[u'ok'] and (ignoreUpdateMsg or res[u'updatedExisting']):
      return data.Result(True, "")
    else:
      return data.Result(False, res[u'err'])

  def get(self, userinfo, documentId):
    """Plugin-Interface: returns a document retrieved from MongoDb """
    mdoc = self.db.getDocument(documentId)
    
    if mdoc is not None:
      return data.Result(True, None, datatrans.mongo2document(mdoc))
    
    return data.Result(False, "Document not found!") #TODO that shouldn't be possible
  
  def save(self, userinfo, documentId, content):
    """Plugin-Interface: stores a document in MongoDb """
    if not self.lock(userinfo, documentId).success:
      return data.Result(False, "Couldn't aquire lock!")
    
    (parent, name) = self.splitDocumentId(documentId)
    now = int(time.time())

    res = self.db.saveDocument(
      documentId,
      {
        'name': name,
        'parent': parent,
        'fullname': documentId,
        'created': {
          'at': now,
          'by': userinfo.id
        },
        'locked': {},
        'content': content,
        'linked': self.extractLinked(content),
        'history': documentId + '_' + str(now)
      }
    )
    return self.fixres(res, True)
  
  def lock(self, userinfo, documentId):
    """Plugin-Interface: locks a document in MongoDb """
    now = int(time.time())

    res = self.db.updateDocument(
      documentId, userinfo.id,
      {
        'locked': {
          'at': now,
          'by': userinfo.id
        }
      }
    )

    return self.fixres(res, False)
  
  def unlock(self, userinfo, documentId):
    """Plugin-Interface: unlocks a document in MongoDb """
    res = self.db.updateDocument(
      documentId, userinfo.id,
      {
        'locked': {}
      }
    )

    return self.fixres(res, False)
