import json


def dictFields(fields):
  def res(c):
    c._encodeFields = fields
    return c
  return res


def toDict(obj):
  if isinstance(obj, (tuple, str, unicode, int, long, float)):
    return obj
  elif isinstance(obj, dict):
    for key in obj.keys():
      obj[key] = toDict(obj[key])
    return obj
  elif isinstance(obj, list):
    return (toDict(el) for el in obj)
  else:
    if hasattr(obj, '_encodeFields'):
      res = {}
      for fieldName in obj._encodeFields:
        res[fieldName] = toDict(obj.__dict__.get(fieldName, None))
      return res

def toJson(obj):
  return json.dumps(toDict(obj))
