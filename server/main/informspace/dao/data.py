import time
from informspace.dao.encoding import dictFields


@dictFields(['success','message','content'])
class Result(object):
  
  def __init__(self, success, message, content=None):
    self.success = success
    self.message = message
    self.content = content


@dictFields(['id'])
class User(object):
  id = None
  auth = None


@dictFields(['at', 'by'])
class Metadata(object):

  def __init__(self):
    self.at = None
    self.by = None

  def set(self, at, by):
    self.at = at
    self.by = by


@dictFields(['id', 'name', 'parent', 'fullname', 'content', 'created', 'locked', 'linked', 'history'])
class Document(object):
  id = None
  name = None
  parent = None
  fullname = None
  
  created = None
  locked = None
  
  content = ""
  linked = []
  
  history = None
