import logging
import uuid

import informspace.pluginmanager as pluginmanager
import informspace.wampexports as exports

from twisted.python import log
from twisted.internet import reactor
from twisted.web.server import Site
from twisted.web.static import File

from autobahn.twisted.websocket import listenWS
from autobahn.wamp1.protocol import \
    WampServerFactory, \
    WampCraProtocol, \
    WampCraServerProtocol, \
    WampServerProtocol


class MyServerProtocol(WampCraServerProtocol):
  authExtraForSession = None
  userInfo = None

  def getAuthSecret(self, authKey):
    userInfoResult = self.authentificationManager.getAuthentificationInformation(authKey)
    self.authExtraForSession = {'salt': str(uuid.uuid4()), 'keylen': 32, 'iterations': 1000}
    
    if userInfoResult != None and userInfoResult.success == True:
      self.userInfo = userInfoResult.content
      return WampCraProtocol.deriveKey(
        str(self.userInfo.auth['hashedSecret']),
        self.authExtraForSession
      )
  
  def getAuthPermissions(self, authKey, authExtra):
    return {
      'permissions': {},
      'authextra': {
        'session': self.authExtraForSession,
        'account': self.userInfo.auth['authExtra']
      }
    }

  def onSessionOpen(self):
    WampCraServerProtocol.onSessionOpen(self)
  
  def onAuthenticated(self, authKey, permissions):
    if authKey and self.userInfo and authKey == str(self.userInfo.auth['authKey']):
      def eventDispatcher(id, event):
        self.dispatch("ws://is/doc/events#" + str(id), event)

      self.registerForRpc(exports.DocIf(
          self.userInfo,
          self.documentStore,
          self.notificationManager,
          eventDispatcher
      ), "ws://is/doc#")
      self.registerForPubSub("ws://is/doc/events#", True, WampServerProtocol.SUBSCRIBE)
    else:
      self.logger.warn("Declining User! Failed authentification: %s : %s", authKey, permissions)


def startServer(plugins, wsport=9000, httpport=None, httpdir=None, debug=False):
  observer = log.PythonLoggingObserver()
  observer.start()
  
  logger = logging.getLogger("wampserv")
  
  (authentificationManager, documentStore, notificationManager) = plugins
  def createServerProtocol():
    p = MyServerProtocol()
    p.logger = logger
    p.authentificationManager = authentificationManager
    p.documentStore = documentStore
    p.notificationManager = notificationManager
    return p

  logger.info("Starting Wamp Server on port %i ...", wsport)

  factory = WampServerFactory("ws://localhost:" + str(wsport),
                              debug=debug,
                              debugCodePaths=debug,
                              debugWamp=debug,
                              debugApp=debug
                              )
  factory.protocol = createServerProtocol
  listenWS(factory)

  if httpport and httpdir:
    logger.info("Starting Http Server on port %i ...", httpport)
    webdir = File(httpdir)
    web = Site(webdir)
    reactor.listenTCP(httpport, web)

  reactor.run()

  logger.warn("Shut down!")

