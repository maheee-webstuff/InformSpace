import logging
import informspace.pluginclasses as pc
import informspace.dao.data as data


class MultiManager(object):
  logger = logging.getLogger("plugin")

  def __init__(self):
    self.managers = []

  def add(self, manager):
    self.managers.append(manager)
    self.managers.sort(key=lambda x: x.priority)


class MultiAuthentificationManager(pc.AuthentificationManagerPlugin, MultiManager):
  def __init__(self):
    MultiManager.__init__(self)
  
  def getAuthentificationInformation(self, authKey):
    for m in self.managers:
      self.logger.debug("Trying Plugin '" + str(m.name) + "' for authKey '" + authKey + "'.")
      authInfo = m.getAuthentificationInformation(authKey)
      if (authInfo != None):
        self.logger.debug("Used Plugin '" + str(m.name) + "' for authKey '" + authKey + "'.")
        return authInfo
    return data.Result(False, "AuthInfo not found!")


class MultiDocumentStore(pc.DocumentStorePlugin, MultiManager):
  def __init__(self):
    MultiManager.__init__(self)

  def getRightDocumentStore(self, documentId):
    for m in self.managers:
      for n in m.namespaces:
        if documentId.startswith(n):
          self.logger.debug("Using Plugin '" + str(m.name) + "' for '" + documentId + "'.")
          return m
    return None
  
  def noStoreFoundError(self):
      self.logger.error("No DocumentStore for '" + str(documentId) + "' found!")
      return data.Result(False, "No DocumentStore for '" + str(documentId) + "' found!")  

  def get(self, userinfo, documentId):
    store = self.getRightDocumentStore(documentId)
    if store:
      return store.get(userinfo, documentId)
    else:
      return self.noStoreFoundError()
  
  def save(self, userinfo, documentId, content):
    store = self.getRightDocumentStore(documentId)
    if store:
      return store.save(userinfo, documentId, content)
    else:
      return self.noStoreFoundError()
  
  def lock(self, userinfo, documentId):
    store = self.getRightDocumentStore(documentId)
    if store:
      return store.lock(userinfo, documentId)
    else:
      return self.noStoreFoundError()
  
  def unlock(self, userinfo, documentId):
    store = self.getRightDocumentStore(documentId)
    if store:
      return store.unlock(userinfo, documentId)
    else:
      return self.noStoreFoundError()


class MultiNotificationManager(pc.NotificationManagerPlugin, MultiManager):
  def __init__(self):
    MultiManager.__init__(self)